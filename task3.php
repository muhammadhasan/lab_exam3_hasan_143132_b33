<!DOCTYPE html>
<html lang="en">
<head>
    <title>Chess Board(8X8)</title>
    <meta charset="utf-8">
    <style>
        *{margin: 0;padding: 0;}
        .main{
            margin: auto;
            width: 800px;
           border-style: solid;
        }

        .box{

            width: 100px;
            height: 100px;
            display: inline-block;

        }

    </style>

</head>
<body>
<div class="main">


    <?php
    for($i=0;$i<8;$i++){

        for($j=0;$j<8;$j++){
            if($i&1){
                if($j&1) {
                    echo "<div style='background-color:black' class=\"box\"></div>";
                }
                else{
                    echo "<div style='background-color:white' class=\"box\"></div>";

                }

            }
            else{
                if($j&1) {
                    echo "<div style='background-color:white' class=\"box\"></div>";
                }
                else{
                    echo "<div style='background-color:black' class=\"box\"></div>";

                }

            }
        }
    }


    ?>

</div>

</body>
</html>